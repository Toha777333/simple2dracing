﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public Transform[] Waypoints;
    public Transform SelfTransform;
    public CarMove Car;
    private int _currentPoint = 0;

	void Update () {
        Transform current = Waypoints[_currentPoint];

        Vector3 direction = SelfTransform.position - current.position;
        float angle = Vector3.Dot(direction, -SelfTransform.right);

        if (angle >= 0)
        {
            Car.RotateRight();
        }
        else
        {
            Car.RotateLeft();
        }

        if (angle < 0.2f && angle > -0.2f)
        {
            Car.Accelerate();
        }

        if (Vector3.Distance(SelfTransform.position, current.position) < 0.2f)
        {
            _currentPoint++;
            if (_currentPoint >= Waypoints.Length)
            {
                _currentPoint = 0;
            }
        }
	}
}
