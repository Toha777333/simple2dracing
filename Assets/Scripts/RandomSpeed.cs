﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpeed : MonoBehaviour {

    public CarMove Car;

	void Start () {
        Car.Speed = Random.Range(4f, 10f);
	}
}
