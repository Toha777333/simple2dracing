﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CarMove : MonoBehaviour {

    public Transform SelfTransform;
    public Tilemap Map;
    public TileBase GroundTile;
    public float Speed;

    private Vector3 _force;
    private bool _isAcceleration;

    public float GetForce()
    {
        return _force.magnitude;
    }
    public string GetTile()
    {
        return Map.GetTile(new Vector3Int((int)SelfTransform.position.x,
            (int)SelfTransform.position.y,
            (int)SelfTransform.position.z)).name + " - " + GroundTile.name;
    }

    public string GetTileXYZ()
    {
        return SelfTransform.position.x + " - " + 
            SelfTransform.position.y + " - " + 
            SelfTransform.position.z;
    }

    public void Accelerate()
    {
        _force += (SelfTransform.up * Time.deltaTime) * 0.1f;
        _isAcceleration = true;
    }
    public void Stop()
    {
        _force = Vector3.Lerp(_force, Vector3.zero, 0.09f);
    }

    public void RotateRight()
    {
        SelfTransform.Rotate(0, 0, -1);
    }

    public void RotateLeft()
    {
        SelfTransform.Rotate(0, 0, 1);
    }

	void LateUpdate () {
        if(!_isAcceleration)
            _force = Vector3.Lerp(_force, Vector3.zero, Time.deltaTime);

        if (GroundTile == Map.GetTile(Map.WorldToCell(SelfTransform.position)))
        {
            _force *= 0.98f;
        }

        SelfTransform.position += _force * Speed * Time.deltaTime;
        
        _isAcceleration = false;
	}
}
