﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow1 : MonoBehaviour {
    public Transform target;
    public Transform SelfTransform;

    private void LateUpdate()
    {
        SelfTransform.position = Vector3.Lerp(SelfTransform.position, target.position + new Vector3(0,0,-10), 0.5f);
    }
}
