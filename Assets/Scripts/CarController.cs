﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    public CarMove Car;

	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            Car.Accelerate();
        }

        if (Input.GetKey(KeyCode.D))
        {
            Car.RotateRight();
        }

        if (Input.GetKey(KeyCode.A))
        {
            Car.RotateLeft();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            Car.Stop();
        }

        if (Input.GetKey(KeyCode.S))
        {
            Car.Stop();
        }
	}
}
